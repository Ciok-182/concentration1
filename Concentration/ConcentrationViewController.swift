//
//  ViewController.swift
//  Concentration
//
//  Created by Ciok on 12/6/17.
//  Copyright © 2017 Ciok. All rights reserved.
//

import UIKit

class ConcentrationViewController: UIViewController {
    
    private lazy var game = Concentration(numberOfPairsOfCards: numberOfPairOfCards)
    
    var numberOfPairOfCards: Int{
        get{
            return (visibleCardButtons.count + 1) / 2
        }
    }
    
   private(set) var flipCount = 0{
        //esto es un observador de la propiedad y cuando cambia se ejecuta el codigo
        didSet{
            updateFlipCountLabel()
        }
    }
    
    func updateFlipCountLabel(){
        let attributes : [NSAttributedStringKey: Any] = [
            .strokeWidth : 5.0,
            .strokeColor : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        ]
        let attributedString = NSAttributedString(
            string: traitCollection.verticalSizeClass == .compact ? "Flips\n \(flipCount)" : "Flips: \(flipCount)",
            attributes: attributes)
        flipCountLabel.attributedText = attributedString
    }
    
    
    //Este se llama cada que tu traitCollection cambia
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateFlipCountLabel()
    }
    
    @IBOutlet private weak var flipCountLabel: UILabel!{
        didSet{
            updateFlipCountLabel()
        }
    }
    
    @IBOutlet private var cardButtons: [UIButton]!
    
    private var visibleCardButtons : [UIButton]!{
        return cardButtons?.filter{!$0.superview!.isHidden}
    }
    
    var theme: String?{
        didSet{
            emojiChoices = theme ?? ""
            emoji = [:]
            updateViewFromModel()
        }
    }
    
    
    private var emojiChoices = "🇧🇷🇦🇷🇫🇷🇪🇨🇧🇴🇨🇴🇮🇹🇯🇲🇲🇽🇳🇬🇺🇾"
    

    private var emoji = [Card:String]()
    private func emoji(for card: Card) -> String{
        
        /* if emoji[card.identifier] == nil{
            if emojiChoices.count > 0 {
        */
        if emoji[card] == nil, emojiChoices.count > 0 {
            let rendomStringIndex = emojiChoices.index(emojiChoices.startIndex, offsetBy: emojiChoices.count.arc4random)
            emoji[card] = String(emojiChoices.remove(at: rendomStringIndex))
        }
        return emoji[card] ?? "?"
        
        /*if emoji[card.identifier] != nil{
            return emoji[card.identifier]!
        }else{
            return "?"
        }*/
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        flipCount += 1
        if let cardNumber = visibleCardButtons.index(of:  sender){
            print("cardNumber = \(cardNumber)")
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        }
        else{
            
            print("Wasn`t cardButtons = ")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewFromModel()
    }
    
    private func updateViewFromModel() {
        if visibleCardButtons != nil {
            for index in visibleCardButtons.indices {
                let button = visibleCardButtons[index]
                let card = game.cards[index]
                if card.isFaceUp{
                    button.setTitle(emoji(for: card), for: UIControlState.normal)
                    button.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                }
                else{
                    button.setTitle("", for: UIControlState.normal)
                    button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 0.07227573544, green: 0.2952378392, blue: 0.8089773655, alpha: 1)
                }
            }
        }
        
    }
}

extension Int {
    var arc4random: Int{
        if self > 0{
            return Int(arc4random_uniform(UInt32(self)))
        }else if self < 0{
            return -Int(arc4random_uniform(UInt32(abs(self))))
        }
        else{
            return 0
        }
    }
}
