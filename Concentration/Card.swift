//
//  Card.swift
//  Concentration
//
//  Created by Jorge Encinas on 26/12/17.
//  Copyright © 2017 Ciok. All rights reserved.
//

import Foundation

struct Card: Hashable {
    //var hashValue: Int {return identifier }
    
    func hash(into hasher: inout Hasher) {
            hasher.combine(identifier)
    }
    
//    public var hashValue: Int {
//           switch self {
//           case .mention: return -1
//           case .hashtag: return -2
//           case .url: return -3
//           case .custom(let regex): return regex.hashValue
//           }
//       }
//
//    func hash(into hasher: inout Hasher) {
//        switch self {
//        case .mention: hasher.combine(-1)
//        case .hashtag: hasher.combine(-2)
//        case .url: hasher.combine(-3)
//        case .custom(let regex): hasher.combine(regex) // assuming regex is a string, that already conforms to hashable
//        }
//    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    var isFaceUp = false
    var isMatched = false
    private var identifier: Int
    
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int{
        identifierFactory += 1
        return identifierFactory
    }
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
}
